# -*- coding: utf-8 -*-
import numpy as np

def ez_volumen(seiten, winkel):
    """
    Berechnet das Volumen einer Elementarzelle.

    Parameter:
    - a, b, c: Längen der drei Seiten der Elementarzelle; aus 'seiten' array
    - alpha, beta, gamma: Winkel zwischen den Seiten in Grad; aus 'winkel' array

    Rückgabe:
    - Volumen der Elementarzelle.
    """
    
    a, b, c = seiten
    alpha, beta, gamma = winkel

    volume = a * b * c * np.sqrt(1 - np.cos(alpha)**2 - np.cos(beta)**2 - np.cos(gamma)**2 +
                                 2 * np.cos(alpha) * np.cos(beta) * np.cos(gamma))
    return volume

#Eingabe der Seitenlängen a, b, c
abc = input('Seitenlängen a b c in Å; z.B. 2 3 4 oder "Enter" für 2 3 4: ')

if abc == '':
    seiten = (2, 3, 4)
else:
    seiten = (float(seite) for seite in abc.split())
    
#Eingabe der Winkel alpha, beta, gamma
al_be_ga = input('Winkel alpha beta gamma in °; z.B. 90 90 90 oder "Enter" für 90 90 90: ')

if al_be_ga  == '':
    winkel = (np.radians(90), np.radians(90), np.radians(90))
else:
    winkel = (np.radians(float(winkel)) for winkel in al_be_ga.split())

#Berechnung des Volumens der Elementarzelle
volumen = ez_volumen(seiten, winkel)

#Ausgbe des Ergebnises
print('Das Volumen der Elementarzelle beträgt', '{:.3f}'.format(volumen), 'Å³.')
